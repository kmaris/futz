import os
import sys

import jinja2
import yaml

from . import config

filters = []
#TODO: Populate the list of filters from class methods and decorators, then
# when it's render time simply pass context.filters to the template env.

def read_file_meta(infile):
    """Load metadata from file object and return a dict.  Return None if no
    metadata header. Empty dict if there was a header but it was empty.
    File cursor will be positioned after the header if there was one."""
    meta = None
    start = infile.tell()
    first_line = infile.readline().decode()
    if first_line.strip() == '---':
        header = first_line
        for line in infile:
            line = line.decode()
            if line.strip():
                header += line
            else:
                break
        meta = yaml.load(header)
    else:
        infile.seek(start)
    return meta

class ContextException(Exception): pass

class Context(object):
    def __init__(self, path, parent=None):
        self.meta = dict()
        self.parent = parent
        self.name = None

        if path:
            self.name = os.path.basename(path)
            if path.endswith(os.sep):
                path = path[:-1]
            if os.path.isdir(path):
                meta_path = os.path.join(path, 'meta.yml')
                if os.path.exists(meta_path):
                    with open(meta_path) as m:
                        self.meta = yaml.load(m)
            elif os.path.basename(path) != 'meta.yml':
                if os.path.exists(path):
                    with open(path, 'rb') as p:
                        self.meta = read_file_meta(p)
            if self.parent is None:
                self.parent = Context(os.path.dirname(path))
        else:
            self.name = ''
            self.meta = config.site()
            path = config.SITE_META
        self.meta['name'] = self.name
        self.meta['path'] = path

    def __getitem__(self, key):
        if key in self.meta:
            return self.meta[key]
        return self.parent[key]

    def __delitem__(self, key):
        if key in self.meta:
            del self.meta[key]
        elif self.parent:
            del self.parent[key]
        else:
            raise KeyError(key, "not found.")

    def __setitem__(self, key, value):
        self.meta[key] = value

    def __iter__(self):
        return iter(self.meta)

    def __str__(self):
        s = self.name + '->' + str(self.meta)
        if self.parent:
            s = '\n'.join([str(self.parent), s])
        return s

    def keys(self):
        if self.parent:
            return list(set(self.parent.keys() + list(self.meta.keys())))
        return list(self.meta.keys())

    def items(self):
        if self.parent:
            p = dict(*self.parent.items())
            p.update(self.meta)
            return p.items()
        return self.meta.items()

    @jinja2.contextfunction
    def url(jinja_context):
        if self.name == '':
            return self.meta['url']
        url = self.name
        if self.parent:
            url = self.parent.url() + url
        return url

if __name__ == '__main__':
    c = Context(sys.argv[1])
    print(str(c))
