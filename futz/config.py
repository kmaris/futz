import yaml

__site = None

CONTENT_ROOT = 'content'
SITE_META = 'site.yml'

def site(force_reload=False, defaults=dict()):
    global __site
    if force_reload or __site is None:
        try:
            with open(SITE_META, 'r') as yml:
                __site = yaml.load(yml)
        except OSError:
            __site = dict()
    s = __site.copy()
    s.update(defaults)
    return s
