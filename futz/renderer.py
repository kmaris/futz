import argparse
import os
import sys

import jinja2
import markdown
import yaml

from jinja2 import Template, Environment, FileSystemLoader
from markdown import markdown

from . import config
from .context import Context, read_file_meta

FS_TEMPLATES = ['theme']

def strip_meta(infile):
    start = infile.tell()
    first_line = infile.readline().decode()
    if first_line.strip() == '---':
        for line in infile:
            if line.decode().strip() == '':
                break
    else:
        infile.seek(start)

def load_env(**kwargs):
    env = Environment(loader=FileSystemLoader(FS_TEMPLATES), **kwargs)
    env.globals['site'] = config.site()
    return env

def _render_markdown(src, dst):
    ctx = Context(src)
    with open(src, 'rb') as infile:
        strip_meta(infile) # Context will have the yaml header.
        ctx["content"] = markdown(infile.read().decode())
    ctx["content"] = Template(ctx["content"]).render(**ctx)
    jenv = load_env()
    if 'template' in ctx:
        template = jenv.get_template(ctx['template'] + '.html')
    else:
        template = jenv.get_template('page.html')
    with open(dst, 'wb') as dstfile:
        dstfile.write(template.render(**ctx).encode())

def _render_directory(src, dst):
    ctx = Context(src)
    with open(src, 'rb') as infile:
        ctx["content"] = markdown(infile.read().decode())
    ctx["content"] = Template(ctx["content"]).render(**ctx)
    jenv = load_env()
    if 'template' in ctx:
        template = jenv.get_template(ctx['template'] + '.html')
    else:
        template = jenv.get_template('index.html')
    with open(os.path.join(dst, 'index.html'), 'wb') as dstfile:
        dstfile.write(template.render(**ctx).encode())

def render(src, dst):
    if src.endswith('.md'):
        render_func = _render_markdown
    else:
        render_func = _render_directory
    render_func(src, dst)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('src', help='the file or directory to render.')
    parser.add_argument('dst', nargs='?', default=sys.stdout,
            help='the destination file or stdout')
    args = parser.parse_args()
    render(args.src, args.dst)
