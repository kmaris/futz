#!/usr/bin/env python

import http.server
import os
import socketserver
import subprocess
import urllib

from .renderer import render_markdown

PORT = 8000

class Futzerver(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.path = '/output' + self.path
        url_parts = urllib.parse.urlsplit(self.path)
        path = self.translate_path(self.path)
        if os.path.isdir(path):
            if not path.endswith('/'):
                path += '/'
        target = os.path.relpath(path)
        subprocess.run(["make", target])
        super().do_GET()

if __name__ == '__main__':
    httpd = socketserver.TCPServer(("", PORT), Futzerver)
    print("Serving on port", PORT)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("\nExiting server.")
    finally:
        httpd.shutdown()
        httpd.server_close()
