---
title: Home
template: index

![hurdur](hurdur.jpg)

kmaris.net
==========

Welcome to my simple little site. This is around mostly for testing a little
static website generator I wrote. You can find me in Boulder, Colorado. I tend
to be that guy running barefoot. Try it sometime! It is awesome.
