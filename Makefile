SHELL := /bin/bash
FIND := find

#TODO: Should use variables for content and output.

CONTENT_FIND_OPTS := -type f -and -not -name meta.yml -and -not -name '*_' -or \( -not -type d -empty \)

RENDERER := python -m futz.renderer
SERVER := python -m futz.server
METAS := python -m futz.context

SITE := site.yml $(shell $(FIND) theme -type f)

# Targets mirror the inputs. Heaven help me if there are ever spaces in the paths.
TARGETS := $(shell $(FIND) content $(CONTENT_FIND_OPTS) | sed 's/^content/output/')
TARGETS := $(patsubst %.md,%.html,$(TARGETS))

.PHONY: all clean serve deploy

all: $(TARGETS)

deploy: clean all
	scp -C -r output/* kmaris.net:/var/www/html/

serve: output ; $(SERVER)

clean: ; rm -rf output

output: content ; [ -d output ] || mkdir output

output/%: content/%
	@mkdir -p $(@D)
	[ -f $< ] && cp $< $@ || mkdir $@

.SECONDEXPANSION:
output/%.html: content/%.md $$(shell $(METAS) $$<) $(SITE)
	$(RENDERER) $< $@
